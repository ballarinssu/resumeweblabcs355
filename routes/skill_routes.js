var express = require('express');
var router = express.Router();
var skill_dal = require('../model/skill_dal');

router.get('/all', function(req, res) {
    skill_dal.getAll(function(err, result) {
        if (err)
            res.send(err);
        else
            res.render('skill/skillViewAll', {'result': result});
    });
});

router.get('/', function(req, res) {
    if (req.query.skill_id == null)
        res.send('skill_id is null');
    else {
        skill_dal.getById(req.query.skill_id, function(err, result) {
            if (err)
                res.send(err);
            else
                res.render('skill/skillViewByID', {'result': result});
        });
    }
});

router.get('/add', function (req, res) {
    skill_dal.getAll(function (err, result) {
        if (err)
            res.send(err);
        else
            res.render('skill/skillAdd')
    });
});

router.get('/insert', function(req, res) {
    if (req.query.skill_name == null)
        res.send('Skill name must be provided');
    if (req.query.description == null)
        res.send('Description must be provided');
    var skill = {
        'skill_name': req.query.skill_name,
        'description': req.query.description
    }

    skill_dal.skillInsert(skill, function(err, result) {
        if (err)
            res.send(err);
        else
            res.redirect(302, '/skill/all');
    });

});

// Handles the delete button action when an skill is deleted
router.get('/delete', function(req, res) {
    if (req.query.skill_id == null)
        res.send('skill_id is null');
    else {
        skill_dal.skillDelete(req.query.skill_id, function(err, result){
            if (err)
                res.send(err);
            else
                res.redirect(302, '/skill/all');
        });
    }
});

// Handles the edit button action when an skill is edited
router.get('/edit', function(req, res){
    if (req.query.skill_id == null)
        res.send('An skill_id is required.');
    else {
        skill_dal.edit(req.query.skill_id, function(err, result) {
            res.render('skill/skillUpdate', {skill: result[0][0]});
        });
    }
});

// Handles the update submit action when an edited skill is ready to
// be altered.
router.get('/update', function (req, res) {
    skill_dal.update(req.query, function (err, result) {
        res.redirect(302, '/skill/all');
    });
});

module.exports = router;