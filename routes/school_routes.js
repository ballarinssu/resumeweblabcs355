var express = require('express');
var router = express.Router();
var school_dal = require('../model/school_dal');

router.get('/all', function(req, res){
    school_dal.getAll(function(err, result) {
       if (err)
           res.send(err);
        else
            res.render('school/schoolViewAll', {'result': result})
    });
});

router.get('/', function(req, res) {
    if (req.query.school_id == null)
        res.send('school_id is null');
    else
        school_dal.getById(req.query.school_id, function(err, result){
            if (err)
              res.send(err);
            else
              res.render('school/schoolViewById', {'result': result});
        });
});

router.get('/add', function(req, res){
    school_dal.getAddresses(function(err, result) {
       if (err)
           res.send(err);
       else
           res.render('school/schoolAdd', {'result': result})
    });
});

router.get('/insert', function(req, res){
    if (req.query.school_name == '')
        res.send('School name must be provided.');
    else if (req.query.address_id == null)
        res.send('At least one address must be selected');
    else
        school_dal.insert(req.query, function(err, result){
           if (err)
               res.send(err);
           else
               res.redirect(302, '/school/all');
        });
});

router.get('/delete', function (req, res) {

    if (req.query.school_id == null)
        res.send('school_id is null');
    else
        school_dal.delete(req.query.school_id, function(err, result) {
            if (err)
                res.send(err)
            else
                res.redirect(302, '/school/all');
        });

});

router.get('/edit', function(req, res){
    school_dal.edit(req.query.school_id, function(err, result){
        if (err)
            res.send(err);
        else {
            var school = result[0][0];
            school_dal.getAddresses(function (err, addresses) {
                console.log("pushing update", addresses);
                res.render('school/schoolUpdate', {
                    'school': school,
                    'address': addresses
                });
            });
        }
    });
});

router.get('/update', function(req, res) {
    if (req.query.school_id == null)
        res.send('school_id is null');
    else if (req.query.school_name == '')
        res.send('shcool_name is null');
    else if (req.query.address_id == null)
        res.send('address_id is null');
    else
        school_dal.update(req.query, function(err, result){
            if (err)
                res.send(err);
            else
                res.redirect(302, '/school/all');
        });
});

module.exports = router;