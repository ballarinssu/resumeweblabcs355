var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');

router.get('/all', function(req, res){
    resume_dal.getAll(function(err, result){
        if (err)
            res.send(err);
        else {
            res.render('resume/resumeViewAll', {'result': result});
        }
    });
});

router.get('/add/selectuser', function(req, res) {
    resume_dal.getAllAccounts(function (err, result) {
        if (err)
            res.send(err);
        else
            res.render('resume/resumeSelectUser', {'result': result});
    });
});

router.get('/add', function (req, res) {
    if (req.query.account_id == null)
        res.send("account_id is null");
    else
        resume_dal.getAccountInfo(req.query.account_id, function(err, result){
            if (err)
                res.send(err);
            else {
                var accountData = {
                    'account': result[0],
                    'account_company': result[1],
                    'account_school': result[2],
                    'account_skill': result[3]
                };
                res.render('resume/resumeAdd', accountData);
            }
        });
});

router.get('/delete', function (req, res) {
    resume_dal.delete(req.query.resume_id, function (err, result) {
        res.redirect(302, '/resume/all');
    });
});

router.post('/insert', function (req, res) {
    resume_dal.insert(req.body, function (err, result) {
        if (err)
            res.send(err);
        else {
            console.log(result);
            //res.redirect(302, '/resume/all');
            resume_dal.edit(result.newResumeId, function (err, result) {
                result.was_successful = true;
                res.render('resume/resumeUpdate', result);
            })
        }
    });
});

router.get('/edit', function(req, res){
    if (req.query.resume_id == null)
        res.send("A resume id is required");
    else
        resume_dal.edit(req.query.resume_id, function(err, result){
            res.render('resume/resumeUpdate', result);
        });
});

router.post('/update', function(req, res){
    if (typeof req.body.resume_id === 'undefined')
        res.send("Resume id required");
    else {
        resume_dal.update(req.body, function (err, result) {
            if (err)
                res.send(err);
            else {
                resume_dal.edit(req.body.resume_id, function (err, result) {
                    result.was_successful = true;
                    res.render('resume/resumeUpdate', result)
                });
            }
        });
    }
});

module.exports = router;