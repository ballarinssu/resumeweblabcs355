var express = require('express');
var router = express.Router();
var address_dal = require('../model/address_dal')

router.get('/all', function (req, res) {
    address_dal.getAll(function(err, result) {
       if (err)
           res.send(err);
       else
           res.render('address/addressViewAll', {'result': result});
    });
});

// Routes individual ID requests
router.get('/', function(req, res) {
   if (req.query.address_id == null)
       res.send('address_id is null');
   else {
       address_dal.getById(req.query.address_id, function(err, result) {
          if (err)
              res.send(err);
          else
              res.render('address/addressViewByID', {'result': result});
       });
   }
});

// Routs to the "add" template ejs file
router.get('/add', function(req, res) {
    address_dal.getAll(function (err, result) {
        if (err)
            res.send(err);
        else
            res.render('address/addressAdd')
    });
});

// Handles the submit action when a new address is added
router.get('/insert', function(req, res) {
    if (req.query.street == null)
        res.send('Street must be provided.');
    if (req.query.zip_code == null)
        res.send('Zip code must be provided.');
    var address = {
        'street': req.query.street,
        'zip_code': req.query.zip_code
    }
    address_dal.addressInsert(address, function(err, result){
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            res.redirect(302, '/address/all');
        }
    });
});

// Handles the delete button action when an address is deleted
router.get('/delete', function(req, res) {
    if (req.query.address_id == null)
        res.send('address_id is null');
    else {
        address_dal.addressDelete(req.query.address_id, function(err, result){
            if (err)
                res.send(err);
            else
                res.redirect(302, '/address/all');
        });
    }
});

// Handles the edit button action when an address is edited
router.get('/edit', function(req, res){
   if (req.query.address_id == null)
       res.send('An address_id is required.');
   else {
       address_dal.edit(req.query.address_id, function(err, result) {
          res.render('address/addressUpdate', {address: result[0][0]});
       });
   }
});

// Handles the update submit action when an edited address is ready to
// be altered.
router.get('/update', function (req, res) {
    address_dal.update(req.query, function (err, result) {
       res.redirect(302, '/address/all');
    });
});

module.exports = router;