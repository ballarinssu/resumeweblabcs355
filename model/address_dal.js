var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    var query = 'SELECT * FROM address WHERE address_id=?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};


// Insert a new address with given zip/street GET data
var addressInsert = function(address, callback) {
    var query = 'INSERT INTO address (street, zip_code) VALUES (?)';
    var queryData = [[address.street, address.zip_code]];

    connection.query(query, queryData, function(err, result) {
       callback(err, result);
    });
};

module.exports.addressInsert = addressInsert;

// Delete a single address given its ID
var addressDelete = function(address_id, callback) {
    var query = 'DELETE FROM address WHERE address_id=?';
    var queryData = [address_id];
    
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

module.exports.addressDelete = addressDelete;

// Handles updating a single address
exports.update = function(params, callback) {
    var query = 'UPDATE address SET street=?, zip_code=? WHERE address_id=?';
    var queryData = [params.street, params.zip_code, params.address_id];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

// Handles rendering the edit page for a single address
exports.edit = function(account_id, callback) {
    var query = 'CALL address_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function (err, result) {
       callback(err, result);
    });
};