var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM school';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(school_id, callback) {
    var query = 'SELECT * FROM school s ' +
        'LEFT JOIN address a on a.address_id=s.address_id ' +
        'WHERE s.school_id = ?';

    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getAddresses = function(callback) {
    var query = 'SELECT * FROM address';

    connection.query(query, function(err, result){
        callback(err, result)
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO school (school_name, address_id) ' +
                'VALUES (?)';
    var queryData = [[params.school_name, params.address_id]];
    connection.query(query, queryData, function (err, result) {
       callback(err, result);
    });
};

exports.delete = function(school_id, callback) {
    var query = 'DELETE FROM school WHERE school_id = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

/*

DROP PROCEDURE IF EXISTS school_getinfo;

DELIMITER //
CREATE PROCEDURE school_getinfo (_school_id int)
BEGIN

SELECT s.*, a.street, a.zip_code FROM school s
JOIN address a ON a.address_id = s.address_id
WHERE s.school_id = _school_id;

END //
DELIMITER ;

 */

exports.edit = function(school_id, callback) {
    var query = 'CALL school_getinfo(?)';
    var queryData = [school_id];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE school SET school_name = ?, address_id = ? WHERE school_id = ?';
    var queryData = [params.school_name, params.address_id, params.school_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};