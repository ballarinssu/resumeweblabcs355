var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM skill;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(skill_id, callback) {
    var query = 'SELECT * FROM skill WHERE skill_id=?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

// Insert a new skill
var skillInsert = function(skill, callback) {
    var query = 'INSERT INTO skill (skill_name, description) VALUES (?)';
    var queryData = [[skill.skill_name, skill.description]];

    connection.query(query, queryData, function (err, result) {
        callback (err, result);
    });
};
module.exports.skillInsert = skillInsert;

// Delete a single skill given its ID
var skillDelete = function(skill_id, callback) {
    var query = 'DELETE FROM skill WHERE skill_id=?';
    var queryData = [skill_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

module.exports.skillDelete = skillDelete;


// Handles updating a single skill
exports.update = function(params, callback) {
    var query = 'UPDATE skill SET skill_name=?, description=? WHERE skill_id=?';
    var queryData = [params.skill_name, params.description, params.skill_id];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

// Handles rendering the edit page for a single skill
exports.edit = function(skill_id, callback) {
    var query = 'CALL skill_getinfo(?)';
    var queryData = [skill_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};