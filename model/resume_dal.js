var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = "SELECT r.*, a.first_name, a.last_name " +
                "FROM resume r " +
                "JOIN account a ON a.account_id=r.account_id ORDER BY a.first_name ASC";
    connection.query(query, null, function(err, result){
        callback(err, result);
    });
};

exports.getAllAccounts = function(callback) {
    var query = "SELECT * FROM account";
    connection.query(query, null, function(err, result){
        callback(err, result);
    });
};

exports.getAccountInfo = function(account_id, callback) {
    var query = "CALL account_getinfo(?)";
    connection.query(query, [account_id], function (err, result) {
        callback(err, result);
    });
};

exports.insert = function (params, callback) {
    var query = "INSERT INTO resume (account_id, resume_name) VALUES (?)";
    var resumeData = [params.account_id, params.resume_name];
    connection.query(query, [resumeData], function (err, result) {
        if (err) {
            callback(err, null);
        }else {
            var resume_id = result.insertId;
            var resumeCompanyData = [];

            // Only loop through if there are multiple company_id's
            if (params.company_id.constructor === Array) {
                for (var i = 0; i < params.company_id.length; i++) {
                    resumeCompanyData.push([resume_id, params.company_id[i]]);
                }
            } else {
                resumeCompanyData.push([resume_id, params.company_id]);
            }

            var resumeSchoolData = [];
            if (params.school_id.constructor === Array) {
                for (var i = 0; i < params.school_id.length; i++) {
                    console.log("Pushing " + params.school_id[i] + " into schools\n");
                    resumeSchoolData.push([resume_id, params.school_id[i]]);
                }
            }else {
                resumeSchoolData.push([resume_id, params.school_id]);
            }

            var resumeSkillData = [];
            if (params.skill_id.constructor === Array) {
                for (var i = 0; i < params.skill_id.length; i++) {
                    resumeSkillData.push([resume_id, params.skill_id[i]]);
                }
            } else {
                resumeSkillData.push([resume_id, params.skill_id]);
            }

            var query = "INSERT INTO resume_company (resume_id, company_id) VALUES ?";
            connection.query(query, [resumeCompanyData], function(err, result){
                if (err) {
                    console.log(err);
                    callback(err, null); }
                else {
                    var query = "INSERT INTO resume_school (resume_id, school_id) VALUES ?";
                    connection.query(query, [resumeSchoolData], function(err, result){
                        if (err) {
                            console.log(err);
                            callback(err, null); }
                        else {
                            var query = "INSERT INTO resume_skill (resume_id, skill_id) VALUES ?";
                            connection.query(query, [resumeSkillData], function(err, result){
                                if (err)
                                    console.log(err);
                                result.newResumeId = resume_id;
                                callback(err, result);
                            });
                        }
                    });
                }
            });
        }
    });
};

exports.delete = function (resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    connection.query(query, [resume_id], function (err, result) {
        callback(err, result);
    });
};

exports.edit = function(resume_id, callback) {
    var query = 'SELECT account_id FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];
    connection.query(query, queryData, function(err, result){
        if(err)
            callback(err, null);
        else {
            // Get account info from the return account_id associated
            // with the given resume_id parameter
            exports.getAccountInfo(result[0].account_id, function(err, result){

                // Initialize object to hold all values that the current account has
                // for company, school, and skill
                var editInfo = {
                    account: result[0][0],
                    account_company: result[1],
                    account_school: result[2],
                    account_skill: result[3]
                };

                // Now query for the info that's actually in the given resume_id
                var query = "CALL resume_getinfo(?)";
                connection.query(query, [resume_id], function (err, result) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        editInfo.resume = result[0];
                        editInfo.resume_company = result[1];
                        editInfo.resume_school = result[2];
                        editInfo.resume_skill = result[3];
                        callback(err, editInfo);
                    }
                });

            });
        }
    });
};

var insertCompanies = function(companies, callback) {
    var query = "INSERT INTO resume_company (resume_id, company_id) VALUES ?";
    connection.query(query, [companies], function (err, result) {
        callback(err, result);
    });
};
var insertSchools = function(schools, callback) {
    var query = "INSERT INTO resume_school (resume_id, school_id) VALUES ?";
    connection.query(query, [schools], function (err, result) {
        callback(err, result);
    });
};
var insertSkills = function(skills, callback) {
    var query = "INSERT INTO resume_skill (resume_id, skill_id) VALUES ?";
    connection.query(query, [skills], function (err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {

    var query =  "UPDATE resume SET resume_name = ? WHERE resume_id = ?";
    var queryData = [params.resume_name, params.resume_id];
    connection.query(query, queryData, function (err, result) {

        var resume_id = params.resume_id;

        var resumeCompanyData = [];
        // Only loop through if there are multiple resume_company's
        if (params.resume_company.constructor === Array) {
            for (var i = 0; i < params.resume_company.length; i++) {
                resumeCompanyData.push([resume_id, params.resume_company[i]]);
            }
        } else {
            resumeCompanyData.push([resume_id, params.resume_company]);
        }

        var resumeschoolData = [];
        // Only loop through if there are multiple resume_school's
        if (params.resume_school.constructor === Array) {
            for (var i = 0; i < params.resume_school.length; i++) {
                resumeschoolData.push([resume_id, params.resume_school[i]]);
            }
        } else {
            resumeschoolData.push([resume_id, params.resume_school]);
        }
        
        var resumeskillData = [];
        // Only loop through if there are multiple resume_skill's
        if (params.resume_skill.constructor === Array) {
            for (var i = 0; i < params.resume_skill.length; i++) {
                resumeskillData.push([resume_id, params.resume_skill[i]]);
            }
        } else {
            resumeskillData.push([resume_id, params.resume_skill]);
        }

        // Delete all entries for the resume first
        var query = "CALL resume_delete_all(?)";
        connection.query(query, [params.resume_id], function (err, result) {
            insertCompanies(resumeCompanyData, function (err, result) {
                insertSchools(resumeschoolData, function (err, result) {
                   insertSkills(resumeskillData, function (err, result) {
                       callback(err, result);
                   });
                });
            });
        });
    });
};